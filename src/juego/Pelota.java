package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {
	private double x;
	private double y;
	private double diametro;
	private Color color;
	private double angulo;
	private double velocidad;
	private Image imag;

	public Pelota(double x, double y) {
		this.x = x;
		this.y = y;
		this.color = Color.CYAN;
		this.diametro = 50;
		this.angulo = -Math.PI / 6;
		this.velocidad = 1;
		this.imag = Herramientas.cargarImagen("ball.png");
	}

	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, this.color);

		e.dibujarImagen(imag, x, y, x * 0.1, 0.11);
	}

	public void mover() {
		this.x += Math.cos(angulo) * velocidad;
		this.y += Math.sin(angulo) * velocidad;
	}

	public boolean chocaConEntorno(Entorno e) {
		if (this.x - this.diametro / 2 < 0 || this.x + this.diametro / 2 > e.ancho()) {
			return true;
		}
		if (this.y - this.diametro / 2 < 0 || this.y + this.diametro / 2 > e.alto()) {
			return true;
		}
		return false;
	}

	public void rebotar(Entorno e) {
		if (this.x - this.diametro / 2 < 0 || this.x + this.diametro / 2 > e.ancho()) {
			this.angulo = Math.PI - this.angulo;
		} else {
			this.angulo = -this.angulo;
		}
	}

	public void rebotarBarra(Barra barra) {
		if (this.y + this.diametro / 2 > barra.getY() - barra.getAlto() / 2
				|| this.y - this.diametro / 2 < barra.getY() + barra.getAlto() / 2) {
			this.angulo = -this.angulo;
		} else {
			this.angulo = Math.PI - this.angulo;
		}
	}

	public boolean chocaConBarra(Barra barra) {
		return this.x + this.diametro / 2 > barra.getX() - barra.getAncho() / 2
				&& this.x - this.diametro / 2 < barra.getX() + barra.getAncho() / 2
				&& this.y + this.diametro / 2 > barra.getY() - barra.getAlto() / 2
				&& this.y - this.diametro / 2 < barra.getY() + barra.getAlto() / 2;

	}

	public static void agregarPelota(Pelota p, Pelota[] pelotas) {
		for (int i = 0; i < pelotas.length; i++) {
			if (pelotas[i] == null) {
				pelotas[i] = p;
				return;
			}
		}
	}

}

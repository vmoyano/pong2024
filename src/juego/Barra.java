package juego;

import java.awt.Color;

import entorno.Entorno;

public class Barra {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	
	public Barra(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 200;
		this.alto = 40;
		this.color = Color.red;
	}
	
	public void dibuejar(Entorno e) {
		e.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}
	
	public void moverDerecha(Entorno e) {
		if(this.x + this.ancho/2 < e.ancho())
			this.x += 5 ;
	}
	
	public void moverIzquierda() {
		if(this.x - this.ancho/2 >= 0) {
			this.x -= 5 ;
		}
	}
	
	public Pelota disparar() {
		return new Pelota(this.x, this.y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	

}
